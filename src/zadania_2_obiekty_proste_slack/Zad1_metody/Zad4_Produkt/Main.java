package zadania_2_obiekty_proste_slack.Zad1_metody.Zad4_Produkt;

import zadania_2_obiekty_proste_slack.Zad1_metody.Zad3_Faktura.TypPodatku;

public class Main {
    public static void main(String[] args) {
        /*
        Przypadek użycie 1. Produkty przekazane w konstruktorze

        Produkt[] tablicaProduktow = new Produkt[10];
        tablicaProduktow[0] = new Produkt("Chleb", 12.0, 1, TypPodatku.VAT23);
        tablicaProduktow[1] = new Produkt("Mleko", 2.0, 1, TypPodatku.VAT8);
        tablicaProduktow[2] = new Produkt("Jajka", 1.0, 1, TypPodatku.VAT23);

        Rachunek rachunek = new Rachunek(tablicaProduktow, "Ja", "ktos");

         */
        Produkt chleb = new Produkt("chleb", 21.0, 1, TypPodatku.VAT8);
        Produkt bulka = new Produkt("bulka", 3.0, 5, TypPodatku.VAT8);
        Produkt mleko = new Produkt("mleko", 2.0, 1, TypPodatku.VAT23);
        Produkt jajka = new Produkt("jajko", 1.0, 10, TypPodatku.VAT23);
        Rachunek rachunek = new Rachunek("Ja", "On");

        rachunek.wstawNaWolnaPozycje(chleb);
//        rachunek.wypiszWszystkieProdukty();

        System.out.println();
        rachunek.wstawNaPozycje(3, jajka);
        rachunek.wypiszWszystkieProdukty();

        double cenaRachunku = rachunek.obliczCeneBrutto();
        System.out.println(cenaRachunku);

        System.out.println();
        rachunek.wstawNaWolnaPozycje(bulka);
        rachunek.wstawNaWolnaPozycje(bulka);
        rachunek.wstawNaWolnaPozycje(bulka);
//        rachunek.wypiszWszystkieProdukty();

        System.out.println();
        for (int i = 0; i < 10; i++) {
            rachunek.wstawNaWolnaPozycje(mleko);
        }
//        rachunek.wypiszWszystkieProdukty();
    }
}
