package zadania_2_obiekty_proste_slack.Zad1_metody.Zad4_Produkt;

import java.util.Arrays;

public class Rachunek {
    private Produkt[] produkty = new Produkt[10];
    private String kupujacy, sprzedajacy;

    public Rachunek(Produkt[] produkty, String kupujacy, String sprzedajacy) {
        this.produkty = produkty;
        this.kupujacy = kupujacy;
        this.sprzedajacy = sprzedajacy;
    }

    public Rachunek(String kupujacy, String sprzedajacy) {
        this.kupujacy = kupujacy;
        this.sprzedajacy = sprzedajacy;
    }

    public double obliczCeneBrutto() {
        double suma = 0;
        for (int i = 0; i < produkty.length; i++) {
            if (produkty[i] != null) { // nie moge poznać ceny brutto null'a
                // nie odwołujemy się do metod kiedy obiekt jest null NIE WOLNO
                // dla spróbowania wykomentuj linię sprawdzającą if oraz zamknięcie klamry
                suma += produkty[i].obliczCeneBrutto();
            }
        }
        return suma;
    }

    public void wstawNaWolnaPozycje(Produkt produktKtoryChceWstawic) {
        for (int i = 0; i < produkty.length; i++) {
            if (produkty[i] == null) { // na pozycji i znajduje się null (nic)
                produkty[i] = produktKtoryChceWstawic;
//                break; // kończy pętle
                return; // kończy metodę
            }
        }
        System.out.println("Nie udało się wstawić elementu!");
    }

    public void wstawNaPozycje(int pozycja, Produkt produkt) {
        if (pozycja >= produkty.length || pozycja < 0) {
            System.out.println("Brzydki programista");
            return;
        }
        produkty[pozycja] = produkt;
    }

    public void wypiszWszystkieProdukty() {
        for (int i = 0; i < produkty.length; i++) {
            System.out.println(produkty[i]);
        }
    }

    public Produkt[] getProdukty() {
        return produkty;
    }

    public void setProdukty(Produkt[] produkty) {
        this.produkty = produkty;
    }

    public String getKupujacy() {
        return kupujacy;
    }

    public void setKupujacy(String kupujacy) {
        this.kupujacy = kupujacy;
    }

    public String getSprzedajacy() {
        return sprzedajacy;
    }

    public void setSprzedajacy(String sprzedajacy) {
        this.sprzedajacy = sprzedajacy;
    }

    @Override
    public String toString() {
        return "Rachunek{" +
                "produkty=" + Arrays.toString(produkty) +
                ", kupujacy='" + kupujacy + '\'' +
                ", sprzedajacy='" + sprzedajacy + '\'' +
                '}';
    }
}
