package zadania_2_obiekty_proste_slack.Zad1_metody.Zad4_Produkt;

import zadania_2_obiekty_proste_slack.Zad1_metody.Zad3_Faktura.TypPodatku;

public class Produkt {
    private String nazwa;
    private Double cenaNetto;
    private double ilosc;
    private TypPodatku typPodatku;

    public Produkt(String nazwa, Double cenaNetto, double ilosc, TypPodatku typPodatku) {
        this.nazwa = nazwa;
        this.cenaNetto = cenaNetto;
        this.ilosc = ilosc;
        this.typPodatku = typPodatku;
    }

    public double obliczCeneBrutto() {
        return (1.0 + typPodatku.getPodatek()) * cenaNetto;
    }

    public String getNazwa() {
        return nazwa;
    }

    public Double getCenaNetto() {
        return cenaNetto;
    }

    public double getIlosc() {
        return ilosc;
    }

    public TypPodatku getTypPodatku() {
        return typPodatku;
    }

    @Override
    public String toString() {
        return "Produkt{" +
                "nazwa='" + nazwa + '\'' +
                ", cenaNetto=" + cenaNetto +
                ", ilosc=" + ilosc +
                ", typPodatku=" + typPodatku +
                '}';
    }
}
