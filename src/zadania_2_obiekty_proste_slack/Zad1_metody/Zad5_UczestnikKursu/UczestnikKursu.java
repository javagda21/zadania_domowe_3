package zadania_2_obiekty_proste_slack.Zad1_metody.Zad5_UczestnikKursu;

import java.util.Arrays;

public class UczestnikKursu {
    private String imie, nazwisko;
    private int poziomUmiejetnosci;
    private String[] umiejetnosci;

    public UczestnikKursu(String imie, String nazwisko, int poziomUmiejetnosci, String[] umiejetnosci) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        setPoziomUmiejetnosci(poziomUmiejetnosci);
        this.umiejetnosci = umiejetnosci;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int getPoziomUmiejetnosci() {
        return poziomUmiejetnosci;
    }

    public void setPoziomUmiejetnosci(int poziomUmiejetnosci) {
        if (poziomUmiejetnosci > 10) {
            poziomUmiejetnosci = 10;
            System.out.println("Poziom umiejetnosci nie powinien byc wyzszy niz 10");
        } else if (poziomUmiejetnosci < 0) {
            poziomUmiejetnosci = 0;
            System.out.println("Poziom umiejetnosci nie powinien byc nizszy niz 0");
        }
        this.poziomUmiejetnosci = poziomUmiejetnosci;
    }

    public String[] getUmiejetnosci() {
        return umiejetnosci;
    }

    public void setUmiejetnosci(String[] umiejetnosci) {
        this.umiejetnosci = umiejetnosci;
    }

    @Override
    public String toString() {
        return "UczestnikKursu{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", poziomUmiejetnosci=" + poziomUmiejetnosci +
                ", umiejetnosci=" + Arrays.toString(umiejetnosci) +
                '}';
    }
}
