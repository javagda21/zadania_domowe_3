package zadania_2_obiekty_proste_slack.Zad1_metody.Zad5_UczestnikKursu;

public class Main {
    public static void main(String[] args) {
        UczestnikKursu[] uczestnikKursus = new UczestnikKursu[3];
        uczestnikKursus[0] = new UczestnikKursu("Pawel", "Gawel", 1, new String[]{"Siedzenie", "Pisanie", "Word", "myszka"});
        uczestnikKursus[1] = new UczestnikKursu("Pawel", "Ktos", 1, new String[]{"kawa", "czipsy", "chleb", "myszka"});
        uczestnikKursus[2] = new UczestnikKursu("Pawel", "cos", 1, new String[]{"googlowanie", "ogladanie filmow", "kodzenie"});

        for (UczestnikKursu uczestnik : uczestnikKursus) {
            System.out.println(uczestnik);
        }
    }
}
