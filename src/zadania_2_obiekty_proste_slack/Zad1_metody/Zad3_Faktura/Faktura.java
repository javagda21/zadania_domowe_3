package zadania_2_obiekty_proste_slack.Zad1_metody.Zad3_Faktura;

public class Faktura {
    private String numer, nipPlatnika, nipWystawiajacego;
    private TypPodatku typPodatku;
    private double cenaNetto;
//    private double cenaBrutto;

    public Faktura(String numer, String nipPlatnika, String nipWystawiajacego, TypPodatku typPodatku, double cenaNetto) {
        this.numer = numer;
        this.nipPlatnika = nipPlatnika;
        this.nipWystawiajacego = nipWystawiajacego;
        this.typPodatku = typPodatku;
        this.cenaNetto = cenaNetto;
    }

    public double obliczCeneBrutto() {
        return (1.0 + typPodatku.getPodatek()) * cenaNetto;
    }

    public String getNumer() {
        return numer;
    }

    public void setNumer(String numer) {
        this.numer = numer;
    }

    public String getNipPlatnika() {
        return nipPlatnika;
    }

    public void setNipPlatnika(String nipPlatnika) {
        this.nipPlatnika = nipPlatnika;
    }

    public String getNipWystawiajacego() {
        return nipWystawiajacego;
    }

    public void setNipWystawiajacego(String nipWystawiajacego) {
        this.nipWystawiajacego = nipWystawiajacego;
    }

    public TypPodatku getTypPodatku() {
        return typPodatku;
    }

    public void setTypPodatku(TypPodatku typPodatku) {
        this.typPodatku = typPodatku;
    }

    public double getCenaNetto() {
        return cenaNetto;
    }

    public void setCenaNetto(double cenaNetto) {
        this.cenaNetto = cenaNetto;
    }

    @Override
    public String toString() {
        return "Faktura{" +
                "numer='" + numer + '\'' +
                ", nipPlatnika='" + nipPlatnika + '\'' +
                ", nipWystawiajacego='" + nipWystawiajacego + '\'' +
                ", typPodatku=" + typPodatku +
                ", cenaNetto=" + cenaNetto +
                '}';
    }
}
