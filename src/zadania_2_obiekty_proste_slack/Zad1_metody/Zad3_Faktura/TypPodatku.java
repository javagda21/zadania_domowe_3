package zadania_2_obiekty_proste_slack.Zad1_metody.Zad3_Faktura;

public enum TypPodatku {
    VAT8(0.08), VAT23(0.23), VAT0(0.0);

    private double podatek;
    TypPodatku(double v) {
        this.podatek = v;
    }

    public double getPodatek() {
        return podatek;
    }
}
