package zadania_2_obiekty_proste_slack.Zad2_proste;

public class Prostokąt {
    private int bokA;
    private int bokB;

    public Prostokąt(int bokA, int bokB) {
        this.bokA = bokA;
        this.bokB = bokB;
    }

    public int getBokA() {
        return bokA;
    }

    public void setBokA(int bokA) {
        this.bokA = bokA;
    }

    public int getBokB() {
        return bokB;
    }

    public void setBokB(int bokB) {
        this.bokB = bokB;
    }

    public int getArea() { // pobierz pole
        return bokA * bokB;
    }
}
