package zadania_2_obiekty_proste_slack.Zad2_proste.Kawa;

public class Kawa {
    private String nazwa;
    private int moc;
    private TypKawy typKawy;

    public Kawa(String nazwa, int moc, TypKawy typKawy) {
        this.nazwa = nazwa;
        setMoc(moc);
        this.typKawy = typKawy;
    }

    public void setMoc(int moc) {
        if (moc > 10) {
            moc = 10;
        } else if (moc < 0) {
            moc = 0;
        }
        this.moc = moc;
    }

    @Override
    public String toString() {
        return "Kawa{" +
                "nazwa='" + nazwa + '\'' +
                ", moc=" + moc +
                ", typKawy=" + typKawy +
                '}';
    }
}
