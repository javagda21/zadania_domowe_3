package zadania_2_obiekty_proste_slack.Zad2_proste.Kawa;

public class Main {
    public static void main(String[] args) {
        Kawa[] kawas = new Kawa[3];
        kawas[0] = new Kawa("A", 10, TypKawy.ROZPUSZCZALNA);
        kawas[1] = new Kawa("WOSEBA", 1, TypKawy.ROZPUSZCZALNA);
        kawas[2] = new Kawa("MCCAFE", 5, TypKawy.SYPANA);

        for (Kawa kawa : kawas) {
            System.out.println(kawa);
        }

    }
}
