package zadania_2_obiekty_proste_slack.Zad2_proste.Pizza;

import java.util.Arrays;

public class Pizza {
    private String nazwa;
    private double cena;
    private String[] dodatki = new String[3];

    public Pizza(String nazwa, double cena, String[] dodatki) {
        this.nazwa = nazwa;
        this.cena = cena;
        this.dodatki = dodatki;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "nazwa='" + nazwa + '\'' +
                ", cena=" + cena +
                ", dodatki=" + Arrays.toString(dodatki) +
                '}';
    }
}
