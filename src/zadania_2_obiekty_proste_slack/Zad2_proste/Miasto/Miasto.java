package zadania_2_obiekty_proste_slack.Zad2_proste.Miasto;

public class Miasto {
    private String nazwa;
    private Long iloscMieszkancow;
    private double szerGeogr;
    private double dlGeogr;

    public Miasto(String nazwa, Long iloscMieszkancow, double szerGeogr, double dlGeogr) {
        this.nazwa = nazwa;
        this.iloscMieszkancow = iloscMieszkancow;
        this.szerGeogr = szerGeogr;
        this.dlGeogr = dlGeogr;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public void setIloscMieszkancow(Long iloscMieszkancow) {
        this.iloscMieszkancow = iloscMieszkancow;
    }

    public String getNazwa() {
        return nazwa;
    }

    public Long getIloscMieszkancow() {
        return iloscMieszkancow;
    }

    public double getSzerGeogr() {
        return szerGeogr;
    }

    public double getDlGeogr() {
        return dlGeogr;
    }

    @Override
    public String toString() {
        return "Miasto{" +
                "nazwa='" + nazwa + '\'' +
                ", iloscMieszkancow=" + iloscMieszkancow +
                ", szerGeogr=" + szerGeogr +
                ", dlGeogr=" + dlGeogr +
                '}';
    }
}
