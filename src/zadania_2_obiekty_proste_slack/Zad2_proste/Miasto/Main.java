package zadania_2_obiekty_proste_slack.Zad2_proste.Miasto;

public class Main {
    public static void main(String[] args) {
        Miasto[] miasto = new Miasto[3];
        miasto[0] = new Miasto("a", 1L, 2.0, 3.0);
        miasto[1] = new Miasto("Gdynia", 100L, 2.0, 3.0);
        miasto[2] = new Miasto("Gdańsk", 1000L, 2.0, 23.0);

        for (Miasto miasto1 : miasto) {
            System.out.println(miasto1);
        }

    }
}
