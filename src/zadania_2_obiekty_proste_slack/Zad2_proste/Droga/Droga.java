package zadania_2_obiekty_proste_slack.Zad2_proste.Droga;

public class Droga {
    private String nazwa; // 0 - 10
    private int jakos; // 0 - 10
    private TypDrogi typ;
    private boolean czyPrzejezdna;
    private boolean czyRemontowana;
    private double dlugosc;

    public Droga(String nazwa, int jakos, TypDrogi typ, boolean czyPrzejezdna, boolean czyRemontowana, double dlugosc) {
        this.nazwa = nazwa;
//        this.jakos = jakos;
        setJakos(jakos);
        this.typ = typ;
        this.czyPrzejezdna = czyPrzejezdna;
        this.czyRemontowana = czyRemontowana;
        this.dlugosc = dlugosc;
    }

    public void setTyp(TypDrogi typ) {
        this.typ = typ;
    }

    public void setJakos(int jakos) {
        if (jakos > 10) { // cokolwiek większe od 10 zmienione będzie na 10
            jakos = 10;
        } else if (jakos < 0) { // cokolwiek mniejsze od 0 zmienione będzie na 0
            jakos = 0;
        }

        this.jakos = jakos;
    }

    public boolean isCzyPrzejezdna() {
        return czyPrzejezdna;
    }

    public void setCzyPrzejezdna(boolean czyPrzejezdna) {
        this.czyPrzejezdna = czyPrzejezdna;
    }

    public boolean isCzyRemontowana() {
        return czyRemontowana;
    }

    public void setCzyRemontowana(boolean czyRemontowana) {
        this.czyRemontowana = czyRemontowana;
    }

    @Override
    public String toString() {
        return "Droga{" +
                "jakos=" + jakos +
                ", typ=" + typ +
                ", czyPrzejezdna=" + czyPrzejezdna +
                ", czyRemontowana=" + czyRemontowana +
                ", dlugosc=" + dlugosc +
                '}';
    }
}
