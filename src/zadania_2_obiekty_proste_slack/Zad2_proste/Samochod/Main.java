package zadania_2_obiekty_proste_slack.Zad2_proste.Samochod;

public class Main {
    public static void main(String[] args) {
        Samochod[] samochods = new Samochod[3];
        samochods[0] = new Samochod("moj", "Czarny", "BMW", 12.0, TypSamochodu.SEDAN);
        samochods[1] = new Samochod("twoj", "Fioletowy", "Mazda", 100.0, TypSamochodu.KABRIO);
        samochods[2] = new Samochod("niczyj", "Czerwony", "Seat", 155.0, TypSamochodu.SUV);

        for (Samochod samochod : samochods) {
            System.out.println(samochod);
        }


    }
}
