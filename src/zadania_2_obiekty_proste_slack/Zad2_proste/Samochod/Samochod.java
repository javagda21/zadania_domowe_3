package zadania_2_obiekty_proste_slack.Zad2_proste.Samochod;

public class Samochod {
    private String nazwa;
    private String kolor;
    private String marka;
    private double cena;
    private TypSamochodu typSamochodu;

    public Samochod(String nazwa, String kolor, String marka, double cena, TypSamochodu typSamochodu) {
        this.nazwa = nazwa;
        this.kolor = kolor;
        this.marka = marka;
        this.cena = cena;
        this.typSamochodu = typSamochodu;
    }

    @Override
    public String toString() {
        return "Samochod{" +
                "nazwa='" + nazwa + '\'' +
                ", kolor='" + kolor + '\'' +
                ", marka='" + marka + '\'' +
                ", cena=" + cena +
                ", typSamochodu=" + typSamochodu +
                '}';
    }
}
