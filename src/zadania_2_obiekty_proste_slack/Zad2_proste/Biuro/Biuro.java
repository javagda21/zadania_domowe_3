package zadania_2_obiekty_proste_slack.Zad2_proste.Biuro;

public class Biuro {
    private String nazwa;
    private double wielkosc;
    private boolean czyPosiadaKlimatyzacje;
    private boolean czyPosiadaOtwieralneOkna;
    private boolean czyZajeta;

    public Biuro(String nazwa, double wielkosc, boolean czyPosiadaKlimatyzacje, boolean czyPosiadaOtwieralneOkna, boolean czyZajeta) {
        this.nazwa = nazwa;
        this.wielkosc = wielkosc;
        this.czyPosiadaKlimatyzacje = czyPosiadaKlimatyzacje;
        this.czyPosiadaOtwieralneOkna = czyPosiadaOtwieralneOkna;
        this.czyZajeta = czyZajeta;
    }

    @Override
    public String toString() {
//        String wartoscDoWyp = "";
//        if (czyPosiadaKlimatyzacje) {
//            wartoscDoWyp = " posiada klimatyzacje ";
//        } else {
//            wartoscDoWyp = " ";
//        }
//
//        // skrócona wersja if.
//        // warunek, po czym następuje znak zapytania.
//        // po znaku zapytania wpisujemy wartość która zostanie zwrócona jeśli warunek jest spełniony
//        // po dwukropku wpisujemy wartość która zostanie zwrócona jeśli warunek nie jest spełniony
//        String wartoscDoWypisania = ((czyPosiadaKlimatyzacje) ? " posiada klimatyzacje " : " ");

        return "Biuro " + (czyZajeta ? " zajete " : "") +
                nazwa +
                ((czyPosiadaKlimatyzacje) ? ", posiada klimatyzacje " : "") +
                ((czyPosiadaOtwieralneOkna) ? ", ma otwieralne okna" : "") +
                " jest wielkosci " + wielkosc +
                (wielkosc > 10.0 ? " i jest ogromne" : "") +
                '}';
    }
}
