package zadania_2_obiekty_proste_slack.Zad2_proste.Biuro;

public class Main {
    public static void main(String[] args) {
        Biuro[] biura = new Biuro[3];
        biura[0]= new Biuro("a", 11.0, true, false, true);
        biura[1]= new Biuro("b", 10.0, true, true, false);
        biura[2]= new Biuro("c", 10.0, false, false, true);

        for (Biuro biuro : biura) {
            System.out.println(biuro);
        }


    }
}
