package zadania_2_obiekty_proste_slack.Zad2_proste.Telefon;

public class Main {
    public static void main(String[] args) {
        Telefon[] telefony = new Telefon[3];
        telefony[0]= new Telefon("A", 1, 23.0, 1, "515");
        telefony[1]= new Telefon("B", 2, 213.0, 1, "516");
        telefony[2]= new Telefon("C", 3, 263.0, 1, "517");

        for (int i = 0; i < telefony.length; i++) {
            System.out.println(telefony[i]);
        }
        // te pętle zachowują się jednakowo
        for (Telefon telefon : telefony) {
            System.out.println(telefon);
        }

    }
}
