package zadania_2_obiekty_proste_slack.Zad2_proste.Telefon;

public class Telefon {
    private String nazwaProcesora;//- nazwa procesora
    private int mocObliczeniowa;//- moc obliczeniowa
    private double wielkoscEkranu;//- wielkość telefonu
    private int iloscSlotowSim;//- ilość slotów sim
    private String nrTel;//- numer telefonu

    public Telefon(String nazwaProcesora, int mocObliczeniowa, double wielkoscEkranu, int iloscSlotowSim, String nrTel) {
        this.nazwaProcesora = nazwaProcesora;
        this.mocObliczeniowa = mocObliczeniowa;
        this.wielkoscEkranu = wielkoscEkranu;
        this.iloscSlotowSim = iloscSlotowSim;
        this.nrTel = nrTel;
    }

    public String getNazwaProcesora() {
        return nazwaProcesora;
    }

    public void setNazwaProcesora(String nazwaProcesora) {
        this.nazwaProcesora = nazwaProcesora;
    }

    public int getMocObliczeniowa() {
        return mocObliczeniowa;
    }

    public void setMocObliczeniowa(int mocObliczeniowa) {
        this.mocObliczeniowa = mocObliczeniowa;
    }

    public double getWielkoscEkranu() {
        return wielkoscEkranu;
    }

    public void setWielkoscEkranu(double wielkoscEkranu) {
        this.wielkoscEkranu = wielkoscEkranu;
    }

    public int getIloscSlotowSim() {
        return iloscSlotowSim;
    }

    public void setIloscSlotowSim(int iloscSlotowSim) {
        this.iloscSlotowSim = iloscSlotowSim;
    }

    public String getNrTel() {
        return nrTel;
    }

    public void setNrTel(String nrTel) {
        this.nrTel = nrTel;
    }

    @Override
    public String toString() {
        return "Telefon{" +
                "nazwaProcesora='" + nazwaProcesora + '\'' +
                ", mocObliczeniowa=" + mocObliczeniowa +
                ", wielkoscEkranu=" + wielkoscEkranu +
                ", iloscSlotowSim=" + iloscSlotowSim +
                ", nrTel='" + nrTel + '\'' +
                '}';
    }
}
