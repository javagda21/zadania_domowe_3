package zadania.zad2_enum_rower;

public class Rower {
    private String nazwa;
    private int iloscPrzerzutek;
    private TypRoweru typ;

    public Rower(String nazwa, int iloscPrzerzutek, TypRoweru typ) {
        this.nazwa = nazwa;
        this.iloscPrzerzutek = iloscPrzerzutek;
        this.typ = typ;
    }

    @Override
    public String toString() {
        return "Rower{" +
                "nazwa='" + nazwa + '\'' +
                ", iloscPrzerzutek=" + iloscPrzerzutek +
                ", typ=" + typ +
                '}';
    }

    public void wypiszInformacjeORowerze() {
        System.out.println(this + " - takitam rowerek który ma " + typ.pobierzIloscMiejsc() + " miejsc");
    }
}
