package zadania.zad2_enum_rower;

public class Main {
    public static void main(String[] args) {
        Rower[] rowers = new Rower[3];
        rowers[0] = new Rower("A", 5, TypRoweru.TANDEM);
        rowers[1] = new Rower("B", 7, TypRoweru.TANDEM);
        rowers[2] = new Rower("C", 8, TypRoweru.ROWER);

        for (Rower rower : rowers) {
//            System.out.println(rower);
            rower.wypiszInformacjeORowerze();
        }
    }
}
