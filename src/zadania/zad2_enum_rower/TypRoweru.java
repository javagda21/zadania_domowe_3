package zadania.zad2_enum_rower;

public enum  TypRoweru {
    TANDEM(2),
    ROWER(1);
    private int iloscMiejsc;

    TypRoweru(int iloscMiejsc) {
        this.iloscMiejsc = iloscMiejsc;
    }

    public int pobierzIloscMiejsc() {
        return iloscMiejsc;
    }
}
