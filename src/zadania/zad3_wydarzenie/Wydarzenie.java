package zadania.zad3_wydarzenie;

import zadania.zad2_daty.MojaData;

public class Wydarzenie {
    private static final int OBECNY_ROK = 2019;
    private static final int OBECNY_MIESIAC = 3;
    private static final int OBECNY_DZIEN = 15;
    //
    //
    private String nazwa;
    private MojaData data;

    public Wydarzenie(String nazwa, MojaData data) {
        this.nazwa = nazwa;
        this.data = data;
    }

    public int ilePozostaloLat() {
        return data.getRok() - OBECNY_ROK;
    }

    public int ilePozostaloMiesiecy() {
        int miesiecy = (data.getRok() - OBECNY_ROK) * 12;
        miesiecy += data.getMiesiac() - OBECNY_MIESIAC;
        return miesiecy;
    }

    public int ilePozostaloDni() {
        int dni = ilePozostaloMiesiecy() * 30;
        dni += data.getDzien() - OBECNY_DZIEN;

        return dni;
    }
}
