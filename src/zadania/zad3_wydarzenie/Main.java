package zadania.zad3_wydarzenie;

import zadania.zad2_daty.MojaData;

public class Main {
    public static void main(String[] args) {
        MojaData data = new MojaData(2019, 5, 20);
        Wydarzenie wydarzenie = new Wydarzenie("Późna majówka", data);

        System.out.println(wydarzenie.ilePozostaloDni());
        System.out.println(wydarzenie.ilePozostaloMiesiecy());
        System.out.println(wydarzenie.ilePozostaloLat());

    }
}
