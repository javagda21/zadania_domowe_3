package zadania.zad1_enum_plec;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj imie:");
        String imie = scanner.next();

        System.out.println("Podaj nazwisko:");
        String nazwisko = scanner.next();

        System.out.println("Podaj plec:");
        String plec = scanner.next();
        Plec plecEnum;

        if (plec.startsWith("k")) {
            plecEnum = Plec.KOBIETA;
//        } else {
        } else if (plec.startsWith("m")) {
            plecEnum = Plec.MEZCZYZNA;
        } else {
            plecEnum = Plec.OTHER;
        }

        System.out.println("Podaj pesel:");
        String pesel = scanner.next();

        Obywatel obywatel = new Obywatel(plecEnum, imie, nazwisko, pesel);
        System.out.println(obywatel);


        Plec[] wszystkieOpcje = Plec.values();
        for (Plec plecc : wszystkieOpcje) {
            System.out.println(plecc);
        }

    }
}
