package zadania.Zad4_ksiazka;

public class Ksiazka {
    private String nazwa;
    private String autor;
    private int rokWydania;

    public Ksiazka(String nazwa, String autor, int rokWydania) {
        this.nazwa = nazwa;
        this.autor = autor;
        this.rokWydania = rokWydania;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getRokWydania() {
        return rokWydania;
    }

    public void setRokWydania(int rokWydania) {
        this.rokWydania = rokWydania;
    }

    @Override
    public String toString() {
        return "Ksiazka{" +
                "nazwa='" + nazwa + '\'' +
                ", autor='" + autor + '\'' +
                ", rokWydania=" + rokWydania +
                '}';
    }
}
