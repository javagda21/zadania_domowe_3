package zadania.Zad7_plansza;

import zadania.Zad6_punkt.Punkt;
import zadania.zad1_enum_plec.Plec;

public class Main {
    public static void main(String[] args) {
        Punkt a = new Punkt(5, 10);
        Punkt b = new Punkt(20, -30);

        System.out.println(a);
        System.out.println(b);

        // metoda kiedy jest statyczna
//        System.out.println(Plansza.obliczOdleglosc(a, b));

        // metoda kiedy nie jest statyczna
        Plansza p = new Plansza();
        System.out.println(p.obliczOdleglosc(a, b));
    }
}
