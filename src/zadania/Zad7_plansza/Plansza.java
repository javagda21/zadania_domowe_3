package zadania.Zad7_plansza;

import zadania.Zad6_punkt.Punkt;

public class Plansza {
    public double obliczOdleglosc(Punkt p1, Punkt p2) {
        double roznicaX = p2.getX() - p1.getX();
        roznicaX *= roznicaX;

        double roznicaY = p2.getY() - p1.getY();
        roznicaY *= roznicaY;

        return Math.sqrt(roznicaX + roznicaY);
    }
}
