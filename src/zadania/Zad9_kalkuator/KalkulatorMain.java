package zadania.Zad9_kalkuator;

import java.util.Scanner;

public class KalkulatorMain {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double w = 0.0; // w pamięci

        String polecenie;
        do {
            System.out.println("Podaj akcje (wyjdz/+/-///*)");
            polecenie = scanner.next();

            if (polecenie.equalsIgnoreCase("wyjdz")) {
                break;
            }

            System.out.println("Podaj liczbe a:");
            String a = scanner.next();
            double aWartosc = (a.equalsIgnoreCase("w")) ? w : Double.parseDouble(a);

            System.out.println("Podaj liczbe b:");
            String b = scanner.next();
            double bWartosc = (b.equalsIgnoreCase("w")) ? w : Double.parseDouble(b);

            double wynik = 0.0;
            if (polecenie.equalsIgnoreCase("+")) {
                wynik = aWartosc + bWartosc;
            } else if (polecenie.equalsIgnoreCase("-")) {
                wynik = aWartosc - bWartosc;
            } else if (polecenie.equalsIgnoreCase("*")) {
                wynik = aWartosc * bWartosc;
            } else if (polecenie.equalsIgnoreCase("/")) {
                wynik = aWartosc / bWartosc;
            }
            System.out.println(wynik);

            System.out.println("Czy zapisać w pamięci? (t/n)");
            String odpowiedz = scanner.next();
            if (odpowiedz.equalsIgnoreCase("t")) {
                w = wynik;
            }
        }
        while (!polecenie.equalsIgnoreCase("wyjdz"));
    }
}
