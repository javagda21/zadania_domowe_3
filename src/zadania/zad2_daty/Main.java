package zadania.zad2_daty;

import java.time.LocalDateTime;

public class Main {
    public static void main(String[] args) {
        MojaData data = new MojaData(1993, 3, 12);
        System.out.println(data.podajDate_1());
        System.out.println(data.podajDate_2());
        System.out.println(data.podajDate_3());

        System.out.println();

        data = new MojaData(2013, 5, 8);
        System.out.println(data.podajDate_1());
        System.out.println(data.podajDate_2());
        System.out.println(data.podajDate_3());

        int obecnyRok = LocalDateTime.now().getYear();
        int obecnyMiesiąc = LocalDateTime.now().getMonthValue();
        int obecnyDzien = LocalDateTime.now().getDayOfMonth();
    }
}
