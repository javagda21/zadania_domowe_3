package zadania.zad2_daty;

public class MojaData {
    private int rok;
    private int miesiac;
    private int dzien;

    public MojaData(int rok, int miesiac, int dzien) {
        this.rok = rok;
        this.miesiac = miesiac;
        this.dzien = dzien;
    }

    public String podajDate_1() {
        return dzien + "." + miesiac + "." + rok;
    }

    private String uzupelnijLiczbe(int liczba) {
        if (liczba < 10) {
            return "0" + liczba;
        }
        return String.valueOf(liczba);
    }

    public String podajDate_2() {
        return uzupelnijLiczbe(dzien) + "." + uzupelnijLiczbe(miesiac) + "." + rok;
    }

    public String podajDate_3() {
        return uzupelnijLiczbe(dzien) + " " + zwrocMiesiac(miesiac) + " " + rok;
    }

    private String zwrocMiesiac(int miesiac) {
        switch (miesiac) {
            case 1:
                return "sty";
            case 2:
                return "lut";
            case 3:
                return "mar";
            case 4:
                return "kwi";
            case 5:
                return "maj";
            case 6:
                return "cze";
            case 7:
                return "lip";
            case 8:
                return "sie";
            case 9:
                return "wrz";
            case 10:
                return "paź";
            case 11:
                return "lis";
            case 12:
                return "gru";
            default:
                return "AAAAAAAAAAA!";
        }
    }

    public int getRok() {
        return rok;
    }

    public void setRok(int rok) {
        this.rok = rok;
    }

    public int getMiesiac() {
        return miesiac;
    }

    public void setMiesiac(int miesiac) {
        this.miesiac = miesiac;
    }

    public int getDzien() {
        return dzien;
    }

    public void setDzien(int dzien) {
        this.dzien = dzien;
    }
}
