package zadania.Zad5_biblioteka;

import zadania.Zad4_ksiazka.Ksiazka;

public class Main {
    public static void main(String[] args) {
        Biblioteka biblioteka = new Biblioteka(new Ksiazka[]{
                new Ksiazka("Potop", "Sienkiewicz", 2000),
                new Ksiazka("Java od Podstaw", "Nie wiem kto", 2015),
                new Ksiazka("Krzyzacy", "Sienkiewicz", 2000),
                new Ksiazka("Ogniem", "Sienkiewicz", 2000),
                new Ksiazka("Dzieci z aowidjoaiwfj", "Sienkiewicz", 2000),
                new Ksiazka("Lalka", "Sienkiewicz", 2000),
                new Ksiazka("Encyklopedia", "Sienkiewicz", 2000),
                new Ksiazka("Slownik", "Sienkiewicz", 2000),
                new Ksiazka("Koty i psy", "Sienkiewicz", 2000),
                new Ksiazka("Szydełkowanie", "Sienkiewicz", 2000),

        });

        biblioteka.wyswietlDostepneKsiazki();
        System.out.println();
        biblioteka.wyswietlWypozyczoneKsiazki();

        System.out.println();
        System.out.println(biblioteka.wypozyczKsiazke("potop"));
        System.out.println();
        biblioteka.wyswietlWypozyczoneKsiazki();
        System.out.println();

        System.out.println(biblioteka.wypozyczKsiazke("potop"));
        System.out.println();
        System.out.println(biblioteka.wypozyczKsiazke("slownik"));
        System.out.println();
        biblioteka.wyswietlWypozyczoneKsiazki();
    }
}
