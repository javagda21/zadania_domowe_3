package zadania.Zad5_biblioteka;

import zadania.Zad4_ksiazka.Ksiazka;

public class Biblioteka {
    private Ksiazka[] ksiazkiDoWypozyczenia;
    private Ksiazka[] ksiazkiWypozyczone;

    public Biblioteka(Ksiazka[] ksiazki) {
        this.ksiazkiDoWypozyczenia = ksiazki;
        // druga tablica tej samej wielkości co ksiazki do wypozyczenia
        // nie moge wypozyczyc wiecej ksiazek niz mam na stanie.
        this.ksiazkiWypozyczone = new Ksiazka[ksiazki.length];
    }

    public boolean czyKsiazkaJestDostepna(String nazwa) {
        for (int i = 0; i < ksiazkiDoWypozyczenia.length; i++) {
            if (ksiazkiDoWypozyczenia[i] != null) { // sprawdzam czy pozycja jest zajeta.
                if (ksiazkiDoWypozyczenia[i].getNazwa().equalsIgnoreCase(nazwa)) {
                    return true;
                }
            }
        }
        return false;
    }

    public Ksiazka wypozyczKsiazke(String nazwa) {
        // założenie: mamy książke i nie musimy sprawdzać czy jest do wypozyczenia
        // todo: przenieść ksiazke z tablicy wypozyczone z tablicy do wypozyczenia

        Ksiazka ksiazka = null;
        for (int i = 0; i < ksiazkiDoWypozyczenia.length; i++) {
            if (ksiazkiDoWypozyczenia[i] != null) { // sprawdzam czy pozycja jest zajeta.
                if (ksiazkiDoWypozyczenia[i].getNazwa().equalsIgnoreCase(nazwa)) {
                    ksiazka = ksiazkiDoWypozyczenia[i]; // zapisuje ksiazke
                    ksiazkiDoWypozyczenia[i] = null; // czyszczę miejsce w tablicy
                    break;
                }
            }
        }
        if (ksiazka == null) {
            return null;
        }

        for (int i = 0; i < ksiazkiWypozyczone.length; i++) {
            if (ksiazkiWypozyczone[i] == null) { // szukam wolnej pozycji
                ksiazkiWypozyczone[i] = ksiazka;
                break;
            }
        }

        return ksiazka;
    }

    public void wyswietlWypozyczoneKsiazki() {
        for (int i = 0; i < ksiazkiWypozyczone.length; i++) {
            if (ksiazkiWypozyczone[i] != null) {
                System.out.println(ksiazkiWypozyczone[i]);
            }
        }
    }

    public void wyswietlDostepneKsiazki() {
        for (int i = 0; i < ksiazkiDoWypozyczenia.length; i++) {
            if (ksiazkiDoWypozyczenia[i] != null) {
                System.out.println(ksiazkiDoWypozyczenia[i]);
            }
        }
    }
}
