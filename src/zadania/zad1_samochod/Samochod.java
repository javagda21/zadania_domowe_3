package zadania.zad1_samochod;

import java.util.Objects;

public class Samochod {
    protected double predkosc;
    protected String kolor;
    protected String marka;
    protected int rocznik;

    public Samochod(String kolor, String marka, int rocznik) {
        this.kolor = kolor;
        this.marka = marka;
        this.rocznik = rocznik;
    }

    @Override
    public String toString() {
        return kolor + " " + marka +" rocznik " + rocznik;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Samochod)) return false;
        Samochod samochod = (Samochod) o;
        return rocznik == samochod.rocznik &&
                kolor.equals(samochod.kolor) &&
                marka.equals(samochod.marka);
    }

    @Override
    public int hashCode() {
        return Objects.hash(kolor, marka, rocznik);
    }
}
