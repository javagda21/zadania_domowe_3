package zadania.zad1_samochod;

public class Main {
    public static void main(String[] args) {
        Samochod samochod = new Samochod("red", "a", 1);
        SzybkiSamochod szybkiSamochod= new SzybkiSamochod("red", "a", 1);

        Object o = new Object();

        System.out.println(samochod.equals(szybkiSamochod)); // powinienem = true
        System.out.println(samochod.equals(o)); // powinienem = false
    }
}
